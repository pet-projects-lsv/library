type Mods = Record<string, boolean | string>;
type Additional = string | undefined;

export const classNames = (
  cls: string,
  additional: Additional[] = [],
  mods: Mods = {}
): string => {
  return [
    cls,
    ...additional.filter(Boolean),
    mods &&
      Object.entries(mods)
        .filter(([cls, value]) => Boolean(value))
        .map(([cls]) => cls),
  ].join(" ");
};
