в корень проекта импортировать конфиг хука 
расширение i18n Ally
для вс кода поиск пакпи с исходниками в settings.json "i18n-ally.localesPaths": "./public/locales"
npm i react-i18next@11.15.5 i18next@21.6.11
npm install i18next-http-backend@1.3.2 i18next-browser-languagedetector@6.1.3 --save


import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'ru',
    // переменная ниже, нужна что бы регулировать работы дебагера в разных режимах сборки
    // переменная добавляется из веббпака, так же нужно зарегестрировать в global.d.ts
    // declare const __IS_DEV__: boolean;
    debug: __IS_DEV__,

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },

    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
    },
  });

export default i18n;



