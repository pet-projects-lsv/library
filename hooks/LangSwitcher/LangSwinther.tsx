import { useTranslation } from "react-i18next";
import { classNames } from "../../shared/lib/classNames/classNames";
import { Button, ButtonThemeEnum } from "shared/ui/Button/Button";
import cls from "./LangSwitcher.module.scss";

interface ILangSwitcherProps {
  className?: string;
}

export const LangSwitcher = ({ className }: ILangSwitcherProps) => {
  const { t, i18n } = useTranslation();

  const toggle = async () => {
    i18n.changeLanguage(i18n.language === "ru" ? "en" : "ru");
  };

  return (
    <Button
      onClick={toggle}
      theme={ButtonThemeEnum.CLEAR}
      className={classNames(cls.LangSwitcher, [className])}
    >
      {t("Язык")}
    </Button>
  );
};
