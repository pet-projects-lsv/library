import { FC } from "react";
import { LinkProps } from "react-router-dom";
import { Link } from "react-router-dom";
import { classNames } from "../../helpers/classNames";
import cls from "./AppLink.module.scss";

interface IAppLinkProps extends LinkProps {
  className?: string;
}

export const AppLink: FC<IAppLinkProps> = ({
  children,
  className,
  to,
  ...otherProps
}) => {
  return (
    <Link
      className={classNames(cls.AppLink, [className])}
      to={to}
      {...otherProps}
    >
      {children}
    </Link>
  );
};
