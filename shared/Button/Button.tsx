import { ButtonHTMLAttributes, FC } from "react";
import { classNames } from "shared/lib/classNames/classNames";
import cls from "./Button.module.scss";

export enum ButtonThemeEnum {
    CLEAR = 'clear'
}

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  className: string;
  theme: ButtonThemeEnum
}

export const Button: FC<IButtonProps> = ({
  children,
  className,
  onClick,
  theme,
  ...otherpPops
}) => {
  return (
    <button
      onClick={onClick}
      className={classNames(cls.Button, [className], {[cls[theme]]:true})}
      {...otherpPops}
    >
      {children}
    </button>
  );
};
